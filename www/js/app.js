var appName = 'Ecotrust',
	appVersion = '0.0.1',
	app = new Framework7( {
	    modalTitle: appName,
	    material: true,
		swipePanel: 'left',
		swipePanelCloseOpposite: true,
		panelsCloseByOutside: true,
		swipePanelOnlyClose: true,
		animatePages: false
	} ),
	$$ = Dom7,
	setupStatusInterval = null,
	downloadPagination = {
		page: 1,
		per_page: 15
	},
	db = window.openDatabase( 'ecotrust', '0.0.1', 'EcoTrust', 1000000 ),
	currentUser = {
		id: null,
		remote_id: null,
		name: {
			first: null,
			last: null,
			user: null
		},
		districts: null
	},
	apiBase = 'http://ggrids.com/ecotrust/api/';

$$( '.app-name' ).text( appName );

document.addEventListener( 'deviceready', function() {
	if ( window.localStorage.getItem( 'databaseSetup' ) == null )
		createDatabaseTables();

	if ( window.localStorage.getItem( 'installationRegistered' ) == null )
		registerInstallation();

	else {
		if ( window.localStorage.getItem( 'setOneSetup' ) == null )
			downloadData( 'countries' );

		else {
			if ( window.localStorage.getItem( 'currentUser' ) != null ) {
				currentUser = JSON.parse( window.localStorage.getItem( 'currentUser' ) );

				if ( window.localStorage.getItem( 'technicianSetup' ) == null )
					setupTechnician();
			}

			else
				openLoginScreen();
		}
	}
}, false );

function setSetupStatus( status ) {
	$$( '#setup-loader' ).css( {
		'margin-top' : parseInt( ( $$( window ).height() - 100 ) / 2 ) + 'px'
	} );

	removeSetupStatus();

	app.popup( '#setup-screen', false, true );

	$$( '#setup-status span' ).text( status );

	setupStatusInterval = setInterval( function() {
		var container = $$( '#setup-status em' ),
			length = container.text().length;

		if ( length == 0 )
			container.text( '.' );

		else if ( length == 1 )
			container.text( '..' );

		else if ( length == 2 )
			container.text( '...' )

		else
			container.text( '' );
	}, 500 );
}

function removeSetupStatus() {
	if ( !setupStatusInterval == null || typeof setupStatusInterval != 'undefined' )
		clearInterval( setupStatusInterval );
}

function registerInstallation() {
	setSetupStatus( 'Registering installation' );

	app.popup( '#setup-screen', false, true );

	$$.post(
		apiBase + 'inbound',
		{
			method: 'register_installation',
			version: appVersion,
			device_info: JSON.stringify( device )
		},
		function( response ) {
			var response = JSON.parse( response );

			setTimeout( function() {
				if ( response.result == 'success' ) {
					window.localStorage.setItem( 'token', response.data.token );
					window.localStorage.setItem( 'id', response.data.id );

					window.localStorage.setItem( 'installationRegistered', 1 );

					downloadData( 'countries' );
				}

				else {
					app.alert( 'Server not accessible. Try again later!' );

					setTimeout( function() {
						navigator.app.exit();
					}, 3000 );
				}
			}, 3000 );
		},
		function() {
			app.alert( 'Server not accessible. Try again later!' );

			setTimeout( function() {
				navigator.app.exit();
			}, 3000 );
		}
	);
}

function downloadData( entries ) {
	switch ( entries ) {
		case 'countries' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'countries' );

			setSetupStatus( 'Downloading countries' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_countries',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' )
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.countries.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.countries, function( index, country ) {
									tx.executeSql( 'INSERT INTO countries ( remote_id, code, name ) VALUES ( ?, ?, ? )', [ country.id, country.code, country.name ] );
								} );
							}, null );

						downloadPagination.page = response.countries < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'countries' );

						else
							downloadData( 'districts' );
					}

					else
						downloadData( 'countries' );
				},
				function() {
					download_data( 'countries' );
				}
			);

			break;

		case 'districts' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'districts' );

			setSetupStatus( 'Downloading districts' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_districts',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' )
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.districts.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.districts, function( index, district ) {
									tx.executeSql( 'INSERT INTO districts ( remote_id, country_id, name ) VALUES ( ?, ( SELECT id FROM countries WHERE remote_id = ? ), ? )', [ district.id, district.country, district.name ] );
								} );
							}, null );

						downloadPagination.page = response.districts < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'districts' );

						else
							downloadData( 'technicians' );
					}

					else
						downloadData( 'districts' );
				},
				function() {
					downloadData( 'districts' );
				}
			);

			break;

		case 'technicians' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'technicians' );

			setSetupStatus( 'Downloading technicians' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_technicians',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' )
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.technicians.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.technicians, function( index, technician ) {
									if ( technician.app_access.allowed ) {
										var districtIds;

										if ( technician.location.districts.length == 0 )
											districtIds = JSON.stringify( new Array() );

										else {
											tx.executeSql(
												'SELECT id FROM districts WHERE remote_id IN ( ' + technician.location.districts.join( ', ' ) + ' ) ',
												[],
												function( tx, result ) {
													var ids = new Array();

													for ( i = 0; i < result.rows.length; i++ )
														ids.push( result.rows.item( i ).id );

													districtIds = JSON.stringify( ids );
												},
												function( tx, error ) {
													districtIds = JSON.stringify( new Array() );
												}
											);
										}

										var delayInsertRecord = window.setInterval( function() {
											if ( districtIds != null ) {
												db.transaction( function( tx ) {
													tx.executeSql(
														'INSERT INTO technicians ( remote_id, first_name, last_name, user_name, district_ids, password ) VALUES ( ?, ?, ?, ?, ?, ? )',
														[ technician.id, technician.name.first, technician.name.last, technician.name.user, districtIds, technician.app_access.password ]
													);
												}, null );

												clearInterval( delayInsertRecord );
											}
										}, 50 );
									}
								} );
							}, null );

						downloadPagination.page = response.technicians < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'technicians' );

						else {
							window.localStorage.setItem( 'setOneSetup', 1 );

							openLoginScreen();

							app.closeModal( '#setup-screen', true );
						}
					}

					else
						downloadData( 'technicians' );
				},
				function() {
					downloadData( 'technicians' );
				}
			);

			break;

		case 'sub-counties' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'sub_counties' );

			setSetupStatus( 'Downloading sub-counties' );

			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT remote_id FROM districts', [], function( tx, result ) {
					var districts = new Array();

					for ( i = 0; i < result.rows.length; i++ )
						districts.push( result.rows.item( i ).remote_id );

					$$.post(
						apiBase + 'outbound',
						{
							method: 'get_sub_counties',
							page: downloadPagination.page,
							per_page: downloadPagination.per_page,
							token: window.localStorage.getItem( 'token' ),
							account: currentUser.remote_id,
							districts: districts
						},
						function( response ) {
							var response = JSON.parse( response );

							if ( response.result == 'success' ) {
								if ( response.sub_counties.length > 0 )
									db.transaction( function( tx ) {
										$$.each( response.sub_counties, function( index, sub_county ) {
											tx.executeSql( 'INSERT INTO sub_counties ( remote_id, district_id, name ) VALUES ( ?, ( SELECT id FROM districts WHERE remote_id = ? ), ? )', [ sub_county.id, sub_county.location.district, sub_county.name ] );
										} );
									}, null );

								downloadPagination.page = response.sub_counties < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

								if ( downloadPagination.page > 1 )
									downloadData( 'sub-counties' );

								else
									downloadData( 'villages' );
							}

							else
								downloadData( 'sub-counties' );
						},
						function() {
							downloadData( 'sub-counties' );
						}
					);
				}, function( tx, error ) {
					downloadData( 'sub-counties' );
				} );
			} );

			break;

		case 'villages' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'villages' );

			setSetupStatus( 'Downloading villages' );

			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT remote_id FROM sub_counties', [], function( tx, result ) {
					var sub_counties = new Array();

					for ( i = 0; i < result.rows.length; i++ )
						sub_counties.push( result.rows.item( i ).remote_id );

						$$.post(
							apiBase + 'outbound',
							{
								method: 'get_villages',
								page: downloadPagination.page,
								per_page: downloadPagination.per_page,
								token: window.localStorage.getItem( 'token' ),
								account: currentUser.remote_id,
								sub_counties: sub_counties
							},
							function( response ) {
								var response = JSON.parse( response );

								if ( response.result == 'success' ) {
									if ( response.villages.length > 0 )
										db.transaction( function( tx ) {
											$$.each( response.villages, function( index, village ) {
												tx.executeSql( 'INSERT INTO villages ( remote_id, sub_county_id, name ) VALUES ( ?, ( SELECT id FROM sub_counties WHERE remote_id = ? ), ? )', [ village.id, village.location.sub_county, village.name ] );
											} );
										}, null );

									downloadPagination.page = response.villages < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

									if ( downloadPagination.page > 1 )
										downloadData( 'villages' );

									else
										downloadData( 'banks' );
								}

								else
									downloadData( 'villages' );
							},
							function() {
								downloadData( 'villages' );
							}
						);
				}, function( tx, error ) {
					downloadData( 'villages' );
				} );
			} );

			break;

		case 'banks' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'banks' );

			setSetupStatus( 'Downloading banks' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_banks',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' ),
					account: currentUser.remote_id
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.banks.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.banks, function( index, bank ) {
									tx.executeSql( 'INSERT INTO banks ( remote_id, name ) VALUES ( ?, ? )', [ bank.id, bank.name ] );
								} );
							}, null );

						downloadPagination.page = response.banks < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'banks' );

						else
							downloadData( 'bank-branches' );
					}

					else
						downloadData( 'banks' );
				},
				function() {
					downloadData( 'banks' );
				}
			);

			break;

		case 'bank-branches' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'bank_branches' );

			setSetupStatus( 'Downloading bank branches' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_bank_branches',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' ),
					account: currentUser.remote_id
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.bank_branches.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.bank_branches, function( index, bank_branch ) {
									tx.executeSql( 'INSERT INTO villages ( remote_id, bank_id, name ) VALUES ( ?, ( SELECT id FROM banks WHERE remote_id = ? ), ? )', [ bank_branch.id, bank_branch.bank, bank_branch.name ] );
								} );
							}, null );

						downloadPagination.page = response.bank_branches < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'bank-branches' );

						else
							downloadData( 'tech-specifications' );
					}

					else
						downloadData( 'bank-branches' );
				},
				function() {
					downloadData( 'bank-branches' );
				}
			);

			break;

		case 'tech-specifications' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'tech_specifications' );

			setSetupStatus( 'Downloading tech specifications' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_tech_specifications',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' ),
					account: currentUser.remote_id
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.tech_specifications.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.tech_specifications, function( index, tech_specification ) {
									tx.executeSql( 'INSERT INTO tech_specifications ( remote_id, name ) VALUES ( ?, ? )', [ tech_specification.id, tech_specification.name ] );
								} );
							}, null );

						downloadPagination.page = response.tech_specifications < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'tech-specifications' );

						else
							downloadData( 'tech-specification-descriptions' );
					}

					else
						downloadData( 'tech-specifications' );
				},
				function() {
					downloadData( 'tech-specifications' );
				}
			);

			break;

		case 'tech-specification-descriptions' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'tech_specification_descriptions' );

			setSetupStatus( 'Downloading tech specification descriptions' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_tech_specification_descriptions',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' ),
					account: currentUser.remote_id
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.tech_specification_descriptions.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.tech_specification_descriptions, function( index, tech_specification_description ) {
									tx.executeSql( 'INSERT INTO tech_specification_descriptions ( remote_id, tech_specification_id, name ) VALUES ( ?, ( SELECT id FROM tech_specifications WHERE remote_id = ? ), ? )', [ tech_specification_description.id, tech_specification_description.tech_specification, tech_specification_description.name ] );
								} );
							}, null );

						downloadPagination.page = response.tech_specification_descriptions < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'tech-specification-descriptions' );

						else
							downloadData( 'producer-groups' );
					}

					else
						downloadData( 'tech-specification-descriptions' );
				},
				function() {
					downloadData( 'tech-specification-descriptions' );
				}
			);

			break;

		case 'producer-groups' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'producer_groups' );

			setSetupStatus( 'Downloading producer groups' );

			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT remote_id FROM villages', [], function( tx, result ) {
					var villages = new Array();

					for ( i = 0; i < result.rows.length; i++ )
						villages.push( result.rows.item( i ).remote_id );

						$$.post(
							apiBase + 'outbound',
							{
								method: 'get_producer_groups',
								page: downloadPagination.page,
								per_page: downloadPagination.per_page,
								token: window.localStorage.getItem( 'token' ),
								account: currentUser.remote_id,
								villages: villages
							},
							function( response ) {
								var response = JSON.parse( response );

								if ( response.result == 'success' ) {
									if ( response.producer_groups.length > 0 )
										db.transaction( function( tx ) {
											$$.each( response.producer_groups, function( index, producer_group ) {
												tx.executeSql( 'INSERT INTO producer_groups ( remote_id, village_id, name ) VALUES ( ?, ( SELECT id FROM villages WHERE remote_id = ? ), ? )', [ producer_group.id, producer_group.location.village, producer_group.name ] );
											} );
										}, null );

									downloadPagination.page = response.producer_groups < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

									if ( downloadPagination.page > 1 )
										downloadData( 'producer-groups' );

									else {
										window.localStorage.setItem( 'technicianSetup', 1 );

										setTimeout( function() {
											app.closeModal( '#setup-screen', true );
										}, 2000 );
									}
								}

								else
									downloadData( 'producer-groups' );
							},
							function() {
								downloadData( 'producer-groups' );
							}
						);
				}, function( tx, error ) {
					downloadData( 'producer-groups' );
				} );
			} );

			break;

		case 'producers' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'producers' );

			setSetupStatus( 'Downloading producers' );

			break;
	}
}

function createDatabaseTables() {
	db.transaction( function( tx ) {
		var tables = [ 'settings', 'countries', 'districts', 'technicians' ];

		for ( index = 0; index < tables.length; index++ )
			tx.executeSql( 'DROP TABLE IF EXISTS ' + tables[ index ], [] );

		tx.executeSql( 'CREATE TABLE settings ( id INTEGER PRIMARY KEY AUTOINCREMENT, name CHAR(250), value TEXT )' );
		tx.executeSql( 'CREATE TABLE countries ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, code CHAR(3), name CHAR(160) )' );
		tx.executeSql( 'CREATE TABLE districts ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, country_id INTEGER, name CHAR(160) )' );
		tx.executeSql( 'CREATE TABLE sub_counties ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, district_id INTEGER, name CHAR(160) )' );
		tx.executeSql( 'CREATE TABLE villages ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, sub_county_id INTEGER, name CHAR(160) )' );
		tx.executeSql( 'CREATE TABLE technicians ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, first_name CHAR(80), last_name CHAR(80), user_name CHAR(160), district_ids CHAR(15), password CHAR(160) )' );
		tx.executeSql( 'CREATE TABLE banks ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, name CHAR(160) )' );
		tx.executeSql( 'CREATE TABLE bank_branches ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, bank_id INTEGER, name CHAR(160) )' );
		tx.executeSql( 'CREATE TABLE tech_specifications ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, name CHAR(160) )' );
		tx.executeSql( 'CREATE TABLE tech_specification_descriptions ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, tech_specification_id INTEGER, name CHAR(160) )' );
		tx.executeSql( 'CREATE TABLE producer_groups ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, village_id INTEGER, name CHAR(160) )' );
		tx.executeSql( 'CREATE TABLE producers ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, village_id INTEGER, producer_groups CHAR(160), name CHAR(160) )' );
	}, null );
}

function getRecordCount( table ) {
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT id FROM ' + table, [], function( tx, result ) {
			alert( 'Table ' + table + ' has ' + result.rows.length + ' records' );
		} );
	} );
}

function truncateTable( table ) {
	db.transaction( function( tx ) {
		tx.executeSql( 'DELETE FROM ' + table );
	} );
}

function openLoginScreen() {
	$$( '#login-screen form' ).css( {
		'margin-top' : parseInt( ( $$( window ).height() - 250 ) / 2 ) + 'px'
	} );

	$$( '.login-screen-content form [name="username"], .login-screen-content form [name="password"]' ).val( '' );

	$$( '.login-screen-content form .list-button' ).off( 'click' ).on( 'click', function( event ) {
		event.preventDefault();

		var username = $$( '.login-screen-content form [name="username"]' ),
			password = $$( '.login-screen-content form [name="password"]' );

		if ( username.val().trim().length == 0 )
			username.focus();

		else if ( password.val().trim().length == 0 )
			password.focus();

		else {
			app.showPreloader();

			db.transaction( function( tx ) {
				tx.executeSql(
					'SELECT * FROM technicians WHERE user_name = ? LIMIT 1',
					[ username.val() ],
					function( tx, result ) {
						if ( result.rows.length > 0 ) {
							tx.executeSql(
								'SELECT * FROM technicians WHERE user_name = ? AND password = ? LIMIT 1',
								[ username.val(), password.val() ],
								function( tx, result ) {
									app.hidePreloader();

									if ( result.rows.length > 0 ) {
										var record = result.rows.item( 0 );

										currentUser.id = record.id;
										currentUser.remote_id = record.remote_id;
										currentUser.name.first = record.first_name;
										currentUser.name.last = record.last_name;
										currentUser.name.user = record.user_name;
										currentUser.districts = JSON.parse( record.district_ids );

										window.localStorage.setItem( 'currentUser', JSON.stringify( currentUser ) );

										if ( window.localStorage.getItem( 'technicianSetup' ) == null )
											setupTechnician();

										setTimeout( function() {
											app.closeModal( '#login-screen', true );
										}, 2000 );
									}

									else {
										app.hidePreloader();

										app.alert( 'Wrong password entered. Try again!' );
									}
								},
								null
							);
						}

						else {
							app.hidePreloader();

							app.alert( 'No technician found matching that username. Try again!' );
						}
					},
					null
				);
			} );
		}
	} );

	app.loginScreen( '#login-screen', true );
}

function setupTechnician() {
	if ( window.localStorage.getItem( 'installationMapped' ) == null ) {
		setSetupStatus( 'Mapping installation' );

		$$.post(
			apiBase + 'inbound',
			{
				method: 'map_installation',
				id: window.localStorage.getItem( 'id' ),
				token: window.localStorage.getItem( 'token' ),
				user: currentUser.remote_id
			},
			function( response ) {
				var response = JSON.parse( response );

				setTimeout( function() {
					if ( response.result == 'success' ) {
						window.localStorage.setItem( 'installationMapped', 1 );

						downloadData( 'sub-counties' );
					}

					else {
						app.alert( 'Server not accessible. Try again later!' );

						setTimeout( function() {
							navigator.app.exit();
						}, 3000 );
					}
				}, 3000 );
			},
			function() {
				app.alert( 'Server not accessible. Try again later!' );

				setTimeout( function() {
					navigator.app.exit();
				}, 3000 );
			}
		);
	}

	else
		downloadData( 'sub-counties' );
}
